var messagebox = document.getElementById("messagebox");
var username = document.getElementById("username");
var event_id = document.getElementById("event_id");

var chatcontainer = document.getElementById("chatcontainer");
var conn;
// var key = 'current_user_20';
// current_log = localStorage.getItem(key)
// if (typeof current_log != 'undefined'){
    
//     // username.style.display = "none";
//     // event_id.style.display = "none";

//     // chatcontainer.style.display = "block";
//     console.log("first time");
//     // conn = JSON.parse(current_log);
//     // console.log(conn);
//     // conn2 = Object.create(Connection, conn);
//     // console.log(conn2);
// }

username.addEventListener('keypress', function(evt) {
    if (evt.charCode != 13 || this.value == "")
        return;

    evt.preventDefault();

    var name = this.value;
    this.style.display = "none";

    var event_ID = event_id.value;
    event_id.style.display = "none";

    chatcontainer.style.display = "block";


    conn = new Connection(name, event_ID, "chatwindow", "127.0.0.1:2000");
    // console.log('conn');
    // console.log(conn);
    // localStorage.setItem(key, JSON.stringify(conn));
});

messagebox.addEventListener('keypress', function(evt) {
    if (evt.charCode != 13 || conn == undefined)
        return;

    evt.preventDefault();

    if (this.value == "")
        return;

    conn.sendMsg(this.value);

    this.value = "";
});
