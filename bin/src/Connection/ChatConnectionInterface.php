<?php

namespace Chat\Connection;

interface ChatConnectionInterface
{
    public function getConnection();

    public function getName();
    public function getEventId();

    public function setName($name, $event_id);

    public function sendMsg($sender, $msg);
}
