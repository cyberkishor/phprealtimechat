<?php

namespace Chat\Connection;

use Chat\Repository\ChatRepositoryInterface;
use Ratchet\ConnectionInterface;

class ChatConnection implements ChatConnectionInterface
{
    /**
     * The ConnectionInterface instance
     *
     * @var ConnectionInterface
     */
    private $connection;

    /**
     * The username of this connection
     *
     * @var string
     */
    private $name;


    /**
     * The event id of this connection
     *
     * @var string
     */
    private $event_id;

    /**
     * The ChatRepositoryInterface instance
     *
     * @var ChatRepositoryInterface
     */
    private $repository;

    /**
     * ChatConnection Constructor
     *
     * @param ConnectionInterface     $conn
     * @param ChatRepositoryInterface $repository
     * @param string                  $name
     */
    public function __construct(ConnectionInterface $conn, ChatRepositoryInterface $repository, $name = "", $event_id = "")
    {
        $this->connection = $conn;
        $this->name = $name;
        $this->event_id = $event_id;
        $this->repository = $repository;
    }

    /**
     * Sends a message through the socket
     *
     * @param string $sender
     * @param string $msg
     * @return void
     */
    public function sendMsg($sender, $msg)
    {
        $this->send([
            'action'   => 'message',
            'username' => $sender,
            'msg'      => $msg
        ]);
    }

    /**
     * Get the connection instance
     *
     * @return ConnectionInterface
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Set the name for this connection
     *
     * @param string $name
     * @return void
     */
    public function setName($name, $event_id)
    {
        if ($name === "")
            return;

        // Check if the name exists already
        if ($this->repository->getClientByName($name, $event_id) !== null)
        {
            $this->send([
                'action'   => 'setname',
                'success'  => false,
                'username' => $this->name,
                'event_id' => $this->event_id,
            ]);

            return;
        }

        $this->name = $name;
        $this->event_id = $event_id;

        $this->send([
            'action'   => 'setname',
            'success'  => true,
            'username' => $this->name,
            'event_id' => $this->event_id,
        ]);
    }

    /**
     * Get the username of the connection
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getEventId(){
        return $this->event_id;
    }

    /**
     * Send data through the socket
     *
     * @param  array  $data
     * @return void
     */
    private function send(array $data)
    {
        $this->connection->send(json_encode($data));
    }
}
